package com.xurmo.freecell.card;

/*
 * @Author : Sreejith S
 */


public interface GameDeck {
	
	public void putCard(Card card);
	public Card getCardOnTop();
	
}
