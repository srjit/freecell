package com.xurmo.freecell.card;

/*
 * @Author : Sreejith S
 */


import java.util.ArrayList;
import java.util.List;


public class Cardholder implements GameDeck{
	
	public int ID;
	String type;
	public List<Card> CARDS_ON_DECK = new ArrayList<Card>();
	
	public Cardholder(int iD) {
		this.ID = iD;
		this.type = "GENERAL";
	}
	
	public Cardholder(int iD, String symbolName) {
		this.ID = iD;
		this.type = symbolName;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void putCard(Card card){
		CARDS_ON_DECK.add(card);
	}
	
	public Card getCardOnTop(){
		return CARDS_ON_DECK.get(CARDS_ON_DECK.size()-1);
	}
	
	public Card getCard(int position){
		return CARDS_ON_DECK.get(position);
	}


	public int getID() {
		return ID;
	}


	public void setID(int iD) {
		ID = iD;
	}


	public int getCARD_COUNT() {
		return CARDS_ON_DECK.size();
	}


	public List<Card> getCARDS_ON_DECK() {
		return CARDS_ON_DECK;
	}

}
