package com.xurmo.freecell.card;

/*
 * @Author : Sreejith S
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Game {

	int i;
	String move;
	String[] movementArguments;
	Board board = new Board();
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	public Game() {
		board.initialize();
		board.startGame();
	}
	
	public void playMove(){
		
		if(checkForGameWin()){
			System.out.println("You Win....");
		}
		
		System.out.println("Enter your move[Format - Source cardholder-iD,Destination cardholder-iD || 'q' to Quit Game]  : ");
		
		try {
			move = br.readLine();
			if (move.equals("q")) {
				System.out.println("Quitting game...");
				System.exit(0);
			} else {
				movementArguments = move.split(",");  
				if(movementArguments.length == 2){
					board.nextMove(Integer.parseInt(movementArguments[0]), Integer.parseInt(movementArguments[1]));
				} else {
					System.out.println("Invalid format or Arguments");
				}
			}
			
			playMove();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private boolean checkForGameWin() {
		for(i=6;i<10;i++){
			if(board.cardholders[i].CARDS_ON_DECK.size() != 5)
				return false;
		}
		return true;
	}
	

}
