package com.xurmo.freecell.card;

/*
 * @Author : Sreejith S
 */



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board {

	int i, j = 0, temp;
	String move;
	Card[] gamecards = new Card[20];
	String[] currentCardholders;
	Cardholder[] cardholders = new Cardholder[10];
	Cardholder srcCardholder, destCardholder;

	private Cardholder cardholder;
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	public void initialize() {

		System.out.println("Loading game components......");

		for (i = 2; i <= 6; i++) {
			gamecards[j] = new Card("SPADE", i);
			++j;
		}

		for (i = 2; i <= 6; i++) {
			gamecards[j] = new Card("CLUB", i);
			++j;
		}

		for (i = 2; i <= 6; i++) {
			gamecards[j] = new Card("DIAMOND", i);
			++j;
		}

		for (i = 2; i <= 6; i++) {
			gamecards[j] = new Card("HEART", i);
			++j;
		}

		for (i = 0; i < 4; i++)
			cardholders[i] = new Cardholder(i);

		for (j = 0; j < 4; j++) {
			for (i = 0; i < 5; i++) {
				addToCardHolder(gamecards, cardholders[j], cardholders);
			}
		}

		for (Cardholder cardholder : cardholders) {
			if (cardholder != null) {
				for (Card card : cardholder.CARDS_ON_DECK) {
					System.out
							.println(card.getSymbol() + "***" + card.getNum());
				}
				System.out.println("==================");
			}
		}

		System.out.println("Decks Initialized.....");

	}

	private void addToCardHolder(Card[] gamecards, Cardholder cardholder, Cardholder[] cardholders) {
		Random rand = new Random();
		int temp = rand.nextInt(20);
		boolean flag = true;

		if (cardholders[0].CARDS_ON_DECK.contains(gamecards[temp])) {
			flag = false;
		}

		if (cardholders[1].CARDS_ON_DECK.contains(gamecards[temp])) {
			flag = false;
		}

		if (cardholders[2].CARDS_ON_DECK.contains(gamecards[temp])) {
			flag = false;
		}

		if (cardholders[3].CARDS_ON_DECK.contains(gamecards[temp])) {
			flag = false;
		}

		if (flag) {
			cardholder.CARDS_ON_DECK.add(gamecards[temp]);
		} else {
			addToCardHolder(gamecards, cardholder, cardholders);
		}

	}

	public void startGame() {
		int i, j = 0;

		for (i = 4; i < 6; i++) {
			cardholders[i] = new Cardholder(i);
		}

		System.out.println("Empty Decks Initialized...");

		cardholders[6] = new Cardholder(6, "CLUB");
		cardholders[7] = new Cardholder(7, "SPADE");
		cardholders[8] = new Cardholder(8, "DIAMOND");
		cardholders[9] = new Cardholder(9, "HEART");

		System.out.println("Symbol Holding Decks Initialized...");
		
	}

	public boolean nextMove(int srcID, int destID) {
		
		srcCardholder = cardholders[srcID];
		destCardholder = cardholders[destID];

		if((srcCardholder != null) && (destCardholder != null)){
			if(destID > 5){
				System.out.println("Tring to move to symbol holders...");
				return moveToSymbolHolder(srcCardholder,destCardholder);
			}
			return moveCard(srcCardholder, destCardholder);
		}else {
			System.out.println("Invalid Cardholder IDs");
			return false;
		}

	}
	
	
	public boolean moveCard(Cardholder srcCardholder, Cardholder destCardholder) {
		
		
		srcCardholder.getCardOnTop();
		

		if(destCardholder.CARDS_ON_DECK.size() == 0){
			destCardholder.CARDS_ON_DECK.add(srcCardholder.getCardOnTop());
			srcCardholder.CARDS_ON_DECK.remove(srcCardholder.CARDS_ON_DECK.size() - 1);
			showDecks(srcCardholder, destCardholder);
			return true;
		} 
		
		else if (destCardholder.getCardOnTop().getColor() != srcCardholder.getCardOnTop().getColor()) {
			if ((destCardholder.getCardOnTop().getNum() - srcCardholder.getCardOnTop().getNum()) == 1) {
				destCardholder.CARDS_ON_DECK.add(srcCardholder.getCardOnTop());
				srcCardholder.CARDS_ON_DECK.remove(srcCardholder.CARDS_ON_DECK.size() - 1);
				System.out.println("Card Successfully Moved...");
				showDecks(srcCardholder, destCardholder);
				return true;
			} else {
				System.out.println("That is an Invalid Move.");
				return false;
			} 

		} else {
			System.out.println("That is an Invalid Move(Card colors must be different)");
			return false;
		}
		
		
	}
	
	
	public void showDecks(Cardholder srcCardholder, Cardholder destCardholder){
		System.out.println("\n\nArrangement of deck : "+ srcCardholder.getID() +" after movement");
		for(Card card: srcCardholder.CARDS_ON_DECK){
			if(card != null)
				System.out.println(card.getSymbol() + "***" + card.getNum());
		}
		
		System.out.println("Arrangement of deck : "+ destCardholder.getID() +" after movement");
		for(Card card: destCardholder.CARDS_ON_DECK){
			if(card != null)
				System.out.println(card.getSymbol() + "***" + card.getNum());
		}
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	
	private boolean moveToSymbolHolder(Cardholder srcCardholder, Cardholder destCardholder){
		
		if(srcCardholder.getCardOnTop().getSymbol() == destCardholder.getType()) {
				return addToFinalDeck(srcCardholder,destCardholder);
		}
		
		System.out.println("A " + srcCardholder.getCardOnTop().getSymbol() + " cannot be put on " + destCardholder.getType() + " symbol holder deck.");
		return false;
	}

	
	private boolean addToFinalDeck(Cardholder srcCardholder, Cardholder destCardholder){
		
		if(destCardholder.CARDS_ON_DECK.size() == 0){
			destCardholder.CARDS_ON_DECK.add(srcCardholder.getCardOnTop());
			srcCardholder.CARDS_ON_DECK.remove(srcCardholder.getCardOnTop());
			System.out.println("Card Added to Symbol Holder Deck");
			showDecks(srcCardholder, destCardholder);
			return true;
		} else{
			if(destCardholder.getCardOnTop().getNum() - srcCardholder.getCardOnTop().getNum() == 1){
				destCardholder.CARDS_ON_DECK.add(srcCardholder.getCardOnTop());
				srcCardholder.CARDS_ON_DECK.remove(srcCardholder.getCardOnTop());
				System.out.println("Card Added to Symbol Holder Deck");
				return true;
			}
			return false;
		}
		
	}
	
}
