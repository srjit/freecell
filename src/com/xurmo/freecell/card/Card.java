package com.xurmo.freecell.card;

/*
 * @Author : Sreejith S
 */


public class Card {
	
	String color;
	String symbol = null;
	int num;

	public Card(String symbol, int i) {
		this.symbol = symbol;
		this.num = i;
		if(symbol == "SPADE" || symbol == "CLUB"){
			this.color = "BLACK";
		} else if (symbol == "DIAMOND" || symbol == "HEART"){
			this.color = "RED";
		}
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
	
}
