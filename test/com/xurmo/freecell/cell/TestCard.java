package com.xurmo.freecell.cell;

/*
 * @Author : Sreejith S
 */


import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.Test;

import com.xurmo.freecell.card.Board;
import com.xurmo.freecell.card.Card;
import com.xurmo.freecell.card.Cardholder;
import com.xurmo.freecell.card.Board;
import com.xurmo.freecell.card.Game;

public class TestCard extends TestCase{

	private Cardholder cardholder ;
	private Cardholder cardholder2;
	private Card card;
	private Board setupGame;
	private Game game;

	private void setup() {
	}
	
	@Test
	public void testMakeCard(){
		int i;
		String[] temp = {"SPADE","DIAMOND","CLUB","HEARTS"};
		
		for(String t : temp){
			for(i=2;i<7;i++){
				new Card(t, i);
			}
		}
	}
	
	
	public void testMakeCardHolder(){
		Card card1 = new Card("SPADE", 3);
		Card card2 = new Card("DIAMOND", 2);
		Card card3 = new Card("HEARTS", 5);
		Card card4 = new Card("CLUB", 6);
		Card card5 = new Card("CLUB", 5);
		
		Card card6 = new Card("HEARTS" , 3);
		Card card7 = new Card("DIAMOND" , 6);
		
		
		Card[] cards = null; 
		Cardholder cardholder = new Cardholder(0);
		Cardholder cardholder2 = new Cardholder(1);

		cardholder.putCard(card1);
		cardholder.putCard(card2);
		cardholder.putCard(card3);
		cardholder.putCard(card4);
		cardholder.putCard(card5);
		
		cardholder2.putCard(card6);
		cardholder2.putCard(card7);
		
		Cardholder[] stack = {cardholder,cardholder2};
		
//		Board board = new Board(stack); 
//		board.moveCard(cardholder, cardholder2);
	}
	
	@Test
	public void testBoard(){
		Board board = new Board();
		board.initialize();
		board.startGame();
	}
	
	
	@Test
	public void testGame(){
		game = new Game();
		game.playMove();
		
	}
	
}
